# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from models import Document
from forms import DocumentForm
import subprocess,os

def up(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
	    filename=request.FILES['docfile'].name;
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect('/upload/thanks?file='+filename)
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'list.html',
        {'documents': documents, 'form': form},
        context_instance=RequestContext(request)
    )


def thanks(request):
	name=request.GET['file'];
	absolutepath=os.path.join("/home/vishu/kiwi_repo/kiwi_frost/common/media/mysongs",name);
	args=["madplay","-T",absolutepath,"2>&1"];
	p = subprocess.Popen(args, stdout=subprocess.PIPE,universal_newlines=True)
	##result = p.communicate()[0].decode('ascii');
	##print result
	song="";
	movie="";
	actor="";
	singer="";
	genre="";

	for line in p.stdout:
		line=line.decode('ascii')
		print "**************"
		print line
		if len(line.decode('ascii').strip().split(":"))==1:
				continue
		else:
			field,value=line.strip().split(":")
			print field,value
			if(field=="Title"):
				song=value;
			elif(field=="Artist"):
				singer=value
			elif(field=="Year"):
				year=value
			elif(field=="Album"):
				year=value
			elif(field=="Genre"):
				genre=value


	return render_to_response('thanks.html',
		{'song':song,
		'movie':movie,
		'actor':actor,
		'singer':singer,
		'genre':genre
		},
	context_instance=RequestContext(request))

