from django.http import HttpResponse
from django.shortcuts import render_to_response
import json
import jsonpickle
from django.views.decorators.csrf import csrf_exempt
from middleware import get_csrftoken
# Create your views here.
#TODO refresh access token in certain intervals and dont request FB every time for it
def getFBAccessToken():
    url = 'https://graph.facebook.com/oauth/access_token?client_id=230469030435736&client_secret=d51302e725a24699cae13eb2a71d0468&grant_type=client_credentials'
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    return result['access_token']


def session(request):
    session = {} 
    #request.session
    if request.method == "GET":
        session['_csrf'] = get_csrftoken()
        if 'auth' in request.session :
            session['auth'] = request.session['auth']
        else:
            session['auth'] = request.session['auth'] = "false"
        request.session['_csrf'] = session['_csrf']
        response = HttpResponse(jsonpickle.encode(session,unpicklable=False), mimetype='application/json')
        # should send only the relavent session info
        return response
    elif request.method == "POST" :
        print "holahola"
        print request.POST
        if request.POST['method'] == 'facebook':
            authResponse = authFB(request.POST['authResponse'])
            if authResponse['data']['is_valid'] == "true" :
                session['auth'] = request.session['auth'] = "true"
                session['user_id'] =  request.session['user_id'] = authResponse['data']['user_id']
                session['expires_at'] = request.session['expires_at'] = authResponse['data']['expires_at']
                response = HttpResponse(jsonpickle.encode(session,unpicklable=False), mimetype='application/json')
                return response
            else :
                print "TODO "
                return HttpResponse('')         
                                
        else:
            print "TODO"
            return HttpResponse('')  
        


def authFB(authResponse):
    apptoken = getFBAccessToken()
    usertoken = authResponse['access_token']
    url = 'https://graph.facebook.com/debug_token?input_token=%s&access_token=%s' % usertoken , apptoken
    h = httplib2.Http()
    print url
    response = json.loads(h.request(url, 'GET')[1])
    return response 
    

def login(request):
    
    pass



@csrf_exempt
def sendtogoogle(request):
		  print "inside sendtogoogle";
		  print request;
		  #gplus_id = request.args.get('gplus_id')
		  code = request.POST['code'];
		  print "code"+code;
		  session=request.session;
		  CLIENT_ID="181509372760.apps.googleusercontent.com";
		  try:
		    # Upgrade the authorization code into a credentials object
		    print os.getcwd();
		    clientfile=os.path.join(os.getcwd(),"songs/client_secrets.json");
		    oauth_flow = flow_from_clientsecrets(clientfile, scope='')
		    oauth_flow.redirect_uri = 'postmessage'
		    credentials = oauth_flow.step2_exchange(code)
		  except FlowExchangeError:
		    response = make_response(
		        json.dumps('Failed to upgrade the authorization code.'), 401)
		    response.headers['Content-Type'] = 'application/json'
		    response["Access-Control-Allow-Origin"] = "*"
		    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
		    response["Access-Control-Max-Age"] = "1000"
		    response["Access-Control-Allow-Headers"] = "*"
		    return response

		  # Check that the access token is valid.
		  access_token = credentials.access_token
		  url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
		         % access_token)
		  h = httplib2.Http()
		  result = json.loads(h.request(url, 'GET')[1])
		  print result;
		  # If there was an error in the access token info, abort.
		  if result.get('error') is not None:
		    response = make_response(json.dumps(result.get('error')), 500)
		    response.headers['Content-Type'] = 'application/json'
		    return response
		  # Verify that the access token is used for the intended user.
		  # if result['user_id'] != gplus_id:
		  #   response = make_response(
		  #       json.dumps("Token's user ID doesn't match given user ID."), 401)
		  #   response.headers['Content-Type'] = 'application/json'
		  #   return response
		  # # Verify that the access token is valid for this app.
		  if result['issued_to'] != CLIENT_ID:
		    response = make_response(
		        json.dumps("Token's client ID does not match app's."), 401)
		    response.headers['Content-Type'] = 'application/json'
		    return response
		  stored_credentials = session.get('credentials')
		  # stored_gplus_id = session.get('gplus_id')
		  if stored_credentials is not None:
		    response = HttpResponse(json.dumps('Current user is already connected.'),
		                             200)
		    response.headers['Content-Type'] = 'application/json'
		    return response
		  # Store the access token in the session for later use.
		  session['credentials'] = credentials
		  # session['gplus_id'] = gplus_id

		  response = HttpResponse(json.dumps('Successfully connected user.', 200))
		  response["Access-Control-Allow-Origin"] = "*"
		  response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
		  response["Access-Control-Max-Age"] = "1000"
		  response["Access-Control-Allow-Headers"] = "*"

		  return response;
