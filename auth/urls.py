import settings
from auth import views
#from django.contrib import admin
#from django.conf.urls import handler404
from django.conf.urls import patterns, include, url
#admin.autodiscover()
urlpatterns = patterns('',
    url(r'^gplus/', views.sendtogoogle),
    url(r'^session/', views.session),
)

if settings.DEBUG:
	urlpatterns += patterns('',
     (r'^static/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root': settings.STATIC_ROOT}),
)
