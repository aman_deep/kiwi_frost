from django.db import models

# Create your models here.

class Song(models.Model):
	song_name = models.CharField(max_length=256)
	movie_name = models.CharField(max_length=256)
	song_link = models.CharField(max_length=256)
		
