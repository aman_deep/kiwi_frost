import settings
from home import views
from django.contrib import admin
#from django.conf.urls import handler404
from django.conf.urls import patterns, include, url
admin.autodiscover()
urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^album/', include('album.urls')),
    url(r'^auth/', include('auth.urls')),
    #url(r'^convert', include('lazysignup.urls')),
    url(r'^upload/',include('upload.urls')),
)

if settings.DEBUG:
	urlpatterns += patterns('',
     (r'^static/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root': settings.STATIC_ROOT}),
)
