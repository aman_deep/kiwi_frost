import json
import requests
import jsonpickle
from models import  Album
from django.template.loader import get_template
from django.template import Context
from django.http import Http404, HttpResponse

# will remove following method as template generation for album detail page will reside in kiwi_ice
def albumPage(request, albumId):
    try:
        albumId = int(albumId)
    except ValueError:
        raise Http404()
    # fetch album Details
    response = requests.get("http://127.0.0.1:8000/album/albumDetail/"+str(albumId)) 
    albumSongs = json.loads(response.text)
    firstSong = albumSongs[0]
    albumTitle = firstSong['album_name']
    template=get_template('albumPage.html')
    
    html=template.render(Context({'albumSongs':albumSongs, 'albumTitle':albumTitle, 'albumId':albumId}))
    return HttpResponse(html)

def album_title(request, album_id):
    album = Album.objects.filter(album_id=album_id)
    album = album[0]; 
    response_data={}
    response_data['albumTitle'] = album.album_name
    return HttpResponse(jsonpickle.encode(response_data,unpicklable=False), mimetype='application/json')
    
def album_detail(request, album_id):
    print "am here";
    album = Album.objects.filter(album_id=album_id)
    albumDetail=[]
    for song in album:
        songObject={}
        songObject['song_id']=song.id
        songObject['song_name']=song.song_name
        songObject['song_link']=song.song_link
        songObject['album_name']=song.album_name
        albumDetail.append(songObject)
    
    response=HttpResponse(jsonpickle.encode(albumDetail,unpicklable=False), mimetype='application/json')
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    return  response;
    