from django.conf.urls import patterns, url

from album import views

urlpatterns = patterns('',
   url(r'^(\d+)/$', views.albumPage),
   url(r'^albumTitle/(\d+)/$', views.album_title),
   url(r'^albumDetail/(\d+)/$', views.album_detail),
)
