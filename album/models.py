from django.db import models

# Create your models here.
class Album(models.Model):
    album_id = models.IntegerField()
    album_name = models.CharField(max_length=256)
    song_name = models.CharField(max_length=256)
    song_link = models.CharField(max_length=256)
    class Meta:
        db_table ='songs_album'